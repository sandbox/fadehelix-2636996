/**
 * Provide the HTML to create the modal dialog.
 */
(function ($) {
    Drupal.theme.prototype.feedback_modal = function () {
        var feedbackIco = Drupal.settings['feedback-modal-style'].icoId;
        console.log(feedbackIco);
        var html = '';
        html += '<div id="ctools-modal" class="popups-box">';
        html += '  <div class="top-ico feedback-ico-' + feedbackIco + '"></div>';
        html += '  <div class="ctools-modal-content ctools-modal-feedback-modal-content feedback-modal">';
        html += '       <span class="popups-close"><a class="close" href="#"></a></span>';
        html += '  <div id="modal-content" class="modal-content popups-body "></div>';
        html += '  </div>';
        html += '</div>';
        return html;
    }
})(jQuery);